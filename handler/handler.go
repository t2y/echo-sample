package handler

import (
	"echo-sample/auth"
	"net/http"

	"github.com/labstack/echo"
)

type User struct {
	Name string `json:"name"`
	ID   string `json:"id"`
}

func getUser(c echo.Context) error {
	data := &User{
		Name: c.Get("username").(string),
		ID:   c.Param("id"),
	}
	return c.JSON(http.StatusOK, data)
}

func RegsiterUserHandler(e *echo.Echo) {
	g := e.Group("/user")
	g.Use(auth.NewBasicAuth())
	g.GET("/:id", getUser)
}
