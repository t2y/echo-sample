package auth

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func authenticate(username, password string, c echo.Context) (bool, error) {
	c.Set("username", username)
	if password == "secret" {
		return true, nil
	}
	return false, nil
}

func NewBasicAuth() echo.MiddlewareFunc {
	return middleware.BasicAuth(authenticate)
}
