package auth

import "github.com/labstack/echo"

func Register(e *echo.Echo) {
	e.Use(NewBasicAuth())
}
