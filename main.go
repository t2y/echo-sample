package main

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"echo-sample/handler"
)

func main() {
	e := echo.New()

	// root middleware
	e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
		fmt.Println("response", string(resBody))
	}))

	// root handler
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})

	// user handler
	handler.RegsiterUserHandler(e)
	e.Logger.Fatal(e.Start(":1323"))
}
