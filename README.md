# echo sample application

See [echo guide](https://echo.labstack.com/guide/).

## How to start

```bash
$ go run main.go 

   ____    __
  / __/___/ /  ___
 / _// __/ _ \/ _ \
/___/\__/_//_/\___/ v3.3.10-dev
High performance, minimalist Go web framework
https://echo.labstack.com
____________________________________O/_______
                                    O\
⇨ http server started on [::]:1323
```

## How to test

```bash
$ curl -s localhost:1323/
Hello, World!
```

```bash
$ curl -v localhost:1323/user/123
*   Trying 127.0.0.1:1323...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 1323 (#0)
> GET /user/123 HTTP/1.1
> Host: localhost:1323
> User-Agent: curl/7.68.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 401 Unauthorized
< Content-Type: application/json; charset=UTF-8
< Www-Authenticate: basic realm=Restricted
< Date: Mon, 21 Nov 2022 06:28:09 GMT
< Content-Length: 27
< 
{"message":"Unauthorized"}
* Connection #0 to host localhost left intact
```

```bash
$ curl -v --basic -u t2y:secret localhost:1323/user/123
*   Trying 127.0.0.1:1323...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 1323 (#0)
* Server auth using Basic with user 't2y'
> GET /user/123 HTTP/1.1
> Host: localhost:1323
> Authorization: Basic dDJ5OnNlY3JldA==
> User-Agent: curl/7.68.0
> Accept: */*
>
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< Content-Type: application/json; charset=UTF-8
< Date: Mon, 21 Nov 2022 06:28:36 GMT
< Content-Length: 26
<
{"name":"t2y","id":"123"}
* Connection #0 to host localhost left intact
```
